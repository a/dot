#!/bin/bash
VIDEO_DEVICE="/dev/v4l/by-id/usb-046d_C922_Pro_Stream_Webcam_516C33DF-video-index0"
v4l2-ctl -d "$VIDEO_DEVICE" -c focus_automatic_continuous=0
v4l2-ctl -d "$VIDEO_DEVICE" -c focus_absolute=0
v4l2-ctl -d "$VIDEO_DEVICE" -c zoom_absolute=115
v4l2-ctl -d "$VIDEO_DEVICE" -c power_line_frequency=1
