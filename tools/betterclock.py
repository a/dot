#!/bin/python3
import subprocess
import json


def run_cmd(cmd):
    return subprocess.run(cmd,
                          capture_output=True,
                          shell=True).stdout.decode().strip()


time_pref = "%I:%M %p"

time = run_cmd(f"date '+{time_pref}'")
cal = run_cmd(f"cal --color=always")
# Clean the color output for current date and replace with []
clean_cal = cal.replace(" \u001b[7m", "[").replace("\u001b[27m ", "]")

output_str = json.dumps({"text": time, "tooltip": clean_cal})

print(output_str)
