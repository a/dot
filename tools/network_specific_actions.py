#!/usr/bin/python3
import math
import requests
import time
import sys
import os
import subprocess
import json
from requests.auth import HTTPDigestAuth

mode = sys.argv[1]
demo_mode = len(sys.argv) > 2

with open(os.path.expanduser("~/.config/network_specific_actions_config.json")) as f:
    config = json.load(f)


def _format_minutes(total_minutes: int | float) -> str:
    hours = math.floor(total_minutes / 60)
    mins = int(total_minutes % 60)

    time_delta = f"{hours}h{mins}m" if hours else f"{mins}m"
    return time_delta


def get_wifi_ssid_and_bssid() -> tuple[str, str]:
    if demo_mode:
        if len(sys.argv) > 3:
            return sys.argv[2], sys.argv[3]
        return sys.argv[2], "sample_bssid"

    nmcli_out = subprocess.run(
        'nmcli device wifi list --rescan no | grep "*"',
        shell=True,
        capture_output=True,
        check=True,
    )
    nmcli_stdout = nmcli_out.stdout.decode()
    rows = list(filter(None, nmcli_stdout.split(" ")))
    bssid = rows[1]
    ssid = rows[2]
    return ssid, bssid


class LocationBase:
    user_agent = (
        "Mozilla/5.0 (X11; Linux x86_64; rv:131.0) Gecko/20100101 Firefox/131.0"
    )
    sess = requests.Session()
    sess.headers.update({"User-Agent": user_agent})

    def __init__(self, ssid, bssid):
        self.ssid = ssid
        self.bssid = bssid

    def render(self):
        pass

    def left(self):
        pass


class HomeAssistant(LocationBase):
    # types of entities supporting turn_off/turn_on. might be wrong.
    ha_toggleables = ["switch", "light"]

    def __init__(self, ssid, bssid):
        super().__init__(ssid, bssid)
        ha_config = config["home_assistant"][ssid][bssid if bssid else "any"]
        self.sess.headers.update({"Authorization": f"Bearer {ha_config['api_key']}"})
        self.homeass_url = ha_config["url"]
        self.homeass_devices = ha_config["devices"]

    def get_state(self):
        for entity_id, entity_attributes in self.homeass_devices.items():
            url = f"{self.homeass_url}api/states/{entity_id}"
            req = self.sess.get(url)
            req.raise_for_status()
            entity_attributes["state"] = req.json()["state"]

    def render(self):
        self.get_state()
        output_strs = []

        for entity_attributes in self.homeass_devices.values():
            output_strs.append(
                f"{entity_attributes['name']}: {entity_attributes["state"]}"
            )

        print(" | ".join(output_strs))

    def left(self):
        self.get_state()

        # if any are not on, then we want to turn them on.
        # if all are on, then we want to turn them off.
        # oh and we also want to filter for toggleables
        desired_state = any(
            [
                (
                    entity_attributes.get("state") != "on"
                    if entity_id.split(".")[0] in self.ha_toggleables
                    else False
                )
                for entity_id, entity_attributes in self.homeass_devices.items()
            ]
        )

        action = "turn_on" if desired_state else "turn_off"

        for entity_id, entity_attributes in self.homeass_devices.items():
            entity_type = entity_id.split(".")[0]

            # ignore entities that cannot be toggled
            if entity_type not in self.ha_toggleables:
                continue

            # skip acting on entities that are already on desired state
            if desired_state == (entity_attributes.get("state") == "on"):
                continue

            url = f"{self.homeass_url}api/services/{entity_type}/{action}"
            # print debugging ftw~
            print(url, entity_id)
            req = self.sess.post(
                url,
                json={"entity_id": entity_id},
            )
            req.raise_for_status()


class PrusaLink(LocationBase):
    def __init__(self, ssid, bssid):
        super().__init__(ssid, bssid)
        self.printers = config["prusa_link"][ssid][bssid if bssid else "any"]

    def render(self):
        output_strs = []

        for printer in self.printers:
            url = f"http://{printer['address']}/api/v1/status"
            req = self.sess.get(
                url, auth=HTTPDigestAuth("maker", printer["password"]), timeout=10
            )

            # skip unavailable/broken printers
            if req.status_code > 299:
                continue

            status = req.json()
            progress_text = (
                (
                    f"{int(status['job']['progress'])}% "
                    f"{_format_minutes(status['job']['time_remaining']//60)}"
                )
                if status["printer"]["state"] == "PRINTING"
                else status["printer"]["state"].lower()
            )
            output_strs.append(f"{printer['name']}: {progress_text}")

        print(" | ".join(output_strs))


class DB_ICE(LocationBase):
    api_url_status = "https://iceportal.de/api1/rs/status"
    api_url_tripinfo = "https://iceportal.de/api1/rs/tripInfo/trip"

    examples = {
        # https://gist.github.com/helloworlddan/ac04cab0abf871339fd8b9af95cfa994
        api_url_status: '{"connection":true,"serviceLevel":"AVAILABLE_SERVICE","gpsStatus":"VALID","internet":"UNSTABLE","latitude":49.624525,"longitude":11.0071895,"tileY":-42,"tileX":100,"series":"412","serverTime":1665488184787,"speed":140.0,"trainType":"ICE","tzn":"ICE9008","wagonClass":"SECOND","connectivity":{"currentState":"HIGH","nextState":"UNSTABLE","remainingTimeSeconds":1800},"bapInstalled":true}',
        # https://gist.github.com/helloworlddan/23a4c9d8fcab5fbe557464fb261c7769
        api_url_tripinfo: '{"trip":{"tripDate":"2022-10-11","trainType":"ICE","vzn":"507","actualPosition":630165,"distanceFromLastStop":32263,"totalDistance":831889,"stopInfo":{"scheduledNext":"8001844","actualNext":"8001844","actualLast":"8000025","actualLastStarted":"8001844","finalStationName":"München Hbf","finalStationEvaNr":"8000261"},"stops":[{"station":{"evaNr":"8002553","name":"Hamburg-Altona","code":null,"geocoordinates":{"latitude":53.552695,"longitude":9.935175}},"timetable":{"scheduledArrivalTime":null,"actualArrivalTime":null,"showActualArrivalTime":null,"arrivalDelay":"","scheduledDepartureTime":1665469080000,"actualDepartureTime":1665469125000,"showActualDepartureTime":true,"departureDelay":""},"track":{"scheduled":"12","actual":"12"},"info":{"status":0,"passed":true,"positionStatus":"passed","distance":0,"distanceFromStart":0},"delayReasons":null},{"station":{"evaNr":"8002548","name":"Hamburg Dammtor","code":null,"geocoordinates":{"latitude":53.560751,"longitude":9.989566}},"timetable":{"scheduledArrivalTime":1665469560000,"actualArrivalTime":1665469560000,"showActualArrivalTime":true,"arrivalDelay":"","scheduledDepartureTime":1665469620000,"actualDepartureTime":1665469690000,"showActualDepartureTime":true,"departureDelay":"+1"},"track":{"scheduled":"4","actual":"4"},"info":{"status":0,"passed":true,"positionStatus":"passed","distance":3704,"distanceFromStart":3704},"delayReasons":null},{"station":{"evaNr":"8002549","name":"Hamburg Hbf","code":null,"geocoordinates":{"latitude":53.552736,"longitude":10.006909}},"timetable":{"scheduledArrivalTime":1665469860000,"actualArrivalTime":1665469898000,"showActualArrivalTime":true,"arrivalDelay":"","scheduledDepartureTime":1665470040000,"actualDepartureTime":1665470100000,"showActualDepartureTime":true,"departureDelay":"+1"},"track":{"scheduled":"8","actual":"8"},"info":{"status":0,"passed":true,"positionStatus":"passed","distance":1452,"distanceFromStart":5156},"delayReasons":null},{"station":{"evaNr":"8010404","name":"Berlin-Spandau","code":null,"geocoordinates":{"latitude":52.5346481,"longitude":13.1968975}},"timetable":{"scheduledArrivalTime":1665475680000,"actualArrivalTime":1665475846000,"showActualArrivalTime":true,"arrivalDelay":"+2","scheduledDepartureTime":1665475800000,"actualDepartureTime":1665475928000,"showActualDepartureTime":true,"departureDelay":"+2"},"track":{"scheduled":"5","actual":"5"},"info":{"status":0,"passed":true,"positionStatus":"passed","distance":241476,"distanceFromStart":246632},"delayReasons":null},{"station":{"evaNr":"8098160","name":"Berlin Hbf (tief)","code":null,"geocoordinates":{"latitude":52.525592,"longitude":13.369545}},"timetable":{"scheduledArrivalTime":1665476400000,"actualArrivalTime":1665476639000,"showActualArrivalTime":true,"arrivalDelay":"+3","scheduledDepartureTime":1665477000000,"actualDepartureTime":1665477036000,"showActualDepartureTime":true,"departureDelay":""},"track":{"scheduled":"1","actual":"1"},"info":{"status":0,"passed":true,"positionStatus":"passed","distance":11725,"distanceFromStart":258357},"delayReasons":null},{"station":{"evaNr":"8011113","name":"Berlin Südkreuz","code":null,"geocoordinates":{"latitude":52.475047,"longitude":13.365319}},"timetable":{"scheduledArrivalTime":1665477300000,"actualArrivalTime":1665477366000,"showActualArrivalTime":true,"arrivalDelay":"+1","scheduledDepartureTime":1665477420000,"actualDepartureTime":1665477485000,"showActualDepartureTime":true,"departureDelay":"+1"},"track":{"scheduled":"4","actual":"3"},"info":{"status":0,"passed":true,"positionStatus":"passed","distance":5629,"distanceFromStart":263986},"delayReasons":null},{"station":{"evaNr":"8010222","name":"Lutherstadt Wittenberg Hbf","code":null,"geocoordinates":{"latitude":51.867811,"longitude":12.662285}},"timetable":{"scheduledArrivalTime":1665479400000,"actualArrivalTime":1665479474000,"showActualArrivalTime":true,"arrivalDelay":"+1","scheduledDepartureTime":1665479460000,"actualDepartureTime":1665479550000,"showActualDepartureTime":true,"departureDelay":"+1"},"track":{"scheduled":"2","actual":"2"},"info":{"status":0,"passed":true,"positionStatus":"passed","distance":82834,"distanceFromStart":346820},"delayReasons":null},{"station":{"evaNr":"8010205","name":"Leipzig Hbf","code":null,"geocoordinates":{"latitude":51.3454712,"longitude":12.3820639}},"timetable":{"scheduledArrivalTime":1665481320000,"actualArrivalTime":1665481365000,"showActualArrivalTime":true,"arrivalDelay":"","scheduledDepartureTime":1665481680000,"actualDepartureTime":1665481705000,"showActualDepartureTime":true,"departureDelay":""},"track":{"scheduled":"11","actual":"11"},"info":{"status":0,"passed":true,"positionStatus":"passed","distance":61238,"distanceFromStart":408058},"delayReasons":null},{"station":{"evaNr":"8010101","name":"Erfurt Hbf","code":null,"geocoordinates":{"latitude":50.972551,"longitude":11.038499}},"timetable":{"scheduledArrivalTime":1665484140000,"actualArrivalTime":1665484140000,"showActualArrivalTime":true,"arrivalDelay":"","scheduledDepartureTime":1665484260000,"actualDepartureTime":1665484284000,"showActualDepartureTime":true,"departureDelay":""},"track":{"scheduled":"1","actual":"2"},"info":{"status":0,"passed":true,"positionStatus":"passed","distance":102489,"distanceFromStart":510547},"delayReasons":null},{"station":{"evaNr":"8000025","name":"Bamberg","code":null,"geocoordinates":{"latitude":49.900759,"longitude":10.899489}},"timetable":{"scheduledArrivalTime":1665486900000,"actualArrivalTime":1665486900000,"showActualArrivalTime":true,"arrivalDelay":"","scheduledDepartureTime":1665487020000,"actualDepartureTime":1665487037000,"showActualDepartureTime":true,"departureDelay":""},"track":{"scheduled":"3","actual":"3"},"info":{"status":0,"passed":true,"positionStatus":"passed","distance":119618,"distanceFromStart":630165},"delayReasons":null},{"station":{"evaNr":"8001844","name":"Erlangen","code":null,"geocoordinates":{"latitude":49.5958303,"longitude":11.0016382}},"timetable":{"scheduledArrivalTime":1665488100000,"actualArrivalTime":1665488160000,"showActualArrivalTime":true,"arrivalDelay":"+1","scheduledDepartureTime":1665488220000,"actualDepartureTime":1665488280000,"showActualDepartureTime":true,"departureDelay":"+1"},"track":{"scheduled":"4","actual":"4"},"info":{"status":0,"passed":false,"positionStatus":"arrived","distance":34702,"distanceFromStart":664867},"delayReasons":null},{"station":{"evaNr":"8000284","name":"Nürnberg Hbf","code":null,"geocoordinates":{"latitude":49.445616,"longitude":11.082989}},"timetable":{"scheduledArrivalTime":1665489180000,"actualArrivalTime":1665489240000,"showActualArrivalTime":true,"arrivalDelay":"+1","scheduledDepartureTime":1665489360000,"actualDepartureTime":1665489480000,"showActualDepartureTime":true,"departureDelay":"+2"},"track":{"scheduled":"8","actual":"8"},"info":{"status":0,"passed":false,"positionStatus":"future","distance":17710,"distanceFromStart":682577},"delayReasons":null},{"station":{"evaNr":"8000261","name":"München Hbf","code":null,"geocoordinates":{"latitude":48.140232,"longitude":11.558335}},"timetable":{"scheduledArrivalTime":1665493260000,"actualArrivalTime":1665493260000,"showActualArrivalTime":true,"arrivalDelay":"","scheduledDepartureTime":null,"actualDepartureTime":null,"showActualDepartureTime":null,"departureDelay":""},"track":{"scheduled":"21","actual":"21"},"info":{"status":0,"passed":false,"positionStatus":"future","distance":149312,"distanceFromStart":831889},"delayReasons":null}]},"connection":{"trainType":null,"vzn":null,"trainNumber":null,"station":null,"timetable":null,"track":null,"info":null,"stops":null,"conflict":"NO_CONFLICT"},"active":null}',
    }

    network_status_mapping = {
        "HIGH": ".ıI",
        "MIDDLE": ".ı_",
        "WEAK": ".__",
        "UNSTABLE": "~",
    }
    status_json = None

    def get_endpoint(self, api_url):
        if demo_mode:
            return json.loads(self.examples[api_url])

        req = self.sess.get(api_url)
        req.raise_for_status()
        return req.json()

    def parse_status(self):
        status = self.get_endpoint(self.api_url_status)
        output_strs = []

        self.status_json = status

        # internet status
        network_status_shortcode = self.network_status_mapping.get(
            status["internet"], status["internet"]
        )
        output_strs.append(network_status_shortcode)

        # speed in km/h
        speed = (
            f"{int(status['speed'])}km/h"
            if status["gpsStatus"] == "VALID"
            else "No GPS"
        )
        output_strs.append(speed)
        return output_strs

    def get_time(self):
        # use server time when it's available
        if (
            self.status_json is not None
            and (server_time := self.status_json.get("serverTime")) is not None
        ):
            return int(server_time) / 1000
        else:
            return time.time()

    def parse_tripinfo(self):
        try:
            trip_info = self.get_endpoint(self.api_url_tripinfo)
        # We can do without tripinfo.
        except Exception:
            return []

        train_id = f'{trip_info["trip"]["trainType"]} {trip_info["trip"]["vzn"]}'

        next_stop_evanr = trip_info["trip"]["stopInfo"]["actualNext"]
        for stop in trip_info["trip"]["stops"]:
            if stop["station"]["evaNr"] != next_stop_evanr:
                continue

            stop_name = stop["station"]["name"]
            current_time = self.get_time()
            # if we're waiting for departure
            if stop["info"]["positionStatus"] == "arrived":
                departure_time = stop["timetable"]["actualDepartureTime"]
                departure_delay = stop["timetable"]["departureDelay"]
                # if it is not empty, wrap it in ()
                arrival_delay = (
                    f" ({departure_delay})" if departure_delay else departure_delay
                )
                track = stop["track"]["actual"]

                total_mins_til_departure = ((departure_time / 1000) - current_time) / 60
                departure_time_delta = _format_minutes(total_mins_til_departure)

                text = (
                    f"{stop_name} ({track}) for {departure_time_delta}{departure_delay}"
                )
            # if we're traveling
            else:
                arrival_time = stop["timetable"]["actualArrivalTime"]
                arrival_delay = stop["timetable"]["arrivalDelay"]
                # if it is not empty, wrap it in ()
                arrival_delay = (
                    f" ({arrival_delay})" if arrival_delay else arrival_delay
                )
                track = stop["track"]["actual"]

                total_mins_til_arrival = ((arrival_time / 1000) - current_time) / 60
                arrival_time_delta = _format_minutes(total_mins_til_arrival)

                text = f"{stop_name} ({track}) in {arrival_time_delta}{arrival_delay}"
            return [train_id, text]

    def render(self):
        # Making this explicit order so text is in the right order
        # but also we have status json available to tripinfo
        status_text = self.parse_status()
        output_strs = [*self.parse_tripinfo(), *status_text]

        print(" | ".join(output_strs))


class OEBB_Railjet(LocationBase):
    api_url_speed = "https://railnet.oebb.at/api/speed"

    def get_speed(self):
        if demo_mode:
            return math.floor(float("140.2"))

        req = self.sess.get(self.api_url_speed)
        req.raise_for_status()
        return math.floor(float(req.text))

    def render(self):
        speed = self.get_speed()

        print(f"{speed}km/h")


network_map = {
    "WIFIonICE": DB_ICE,
    "OEBB": OEBB_Railjet,
}
dynamic_config = {
    "home_assistant": HomeAssistant,
    "prusa_link": PrusaLink,
}
for config_key, associated_location_class in dynamic_config.items():
    for config_ssid, config_bssids in config[config_key].items():
        # Add BSSIDs to the list. For "any", add ssid instead.
        for config_bssid in config_bssids:
            network_map[config_bssid if config_bssid != "any" else config_ssid] = (
                associated_location_class
            )

try:
    ssid, bssid = get_wifi_ssid_and_bssid()

    location_class = network_map.get(bssid, network_map.get(ssid, None))

    if location_class:
        # pass an empty BSSID if we didn't use that
        bssid_arg = bssid if bssid in network_map else None
        location = location_class(ssid, bssid_arg)
        if mode == "render":
            location.render()
        elif mode == "left":
            location.left()
    else:
        print("")
except Exception:
    print("")
