#!/bin/bash
CURRENT_BRIGHTNESS=$(cat /sys/class/backlight/*/brightness)
MAX_BRIGHTNESS=$(cat /sys/class/backlight/*/max_brightness)
CHANGE=$(expr $MAX_BRIGHTNESS / $1)
RESULT_BRIGHTNESS=$(expr $CURRENT_BRIGHTNESS + $CHANGE)

if (( $RESULT_BRIGHTNESS > $MAX_BRIGHTNESS )); then
    RESULT_BRIGHTNESS=$MAX_BRIGHTNESS
fi

if (( 0 > $RESULT_BRIGHTNESS )); then
    RESULT_BRIGHTNESS=0
fi

RESULT_PERCENTAGE=$(bc -l <<< "($RESULT_BRIGHTNESS / $MAX_BRIGHTNESS) * 100")
RESULT_PERCENTAGE=${RESULT_PERCENTAGE%.*}
notify-send -a brightness -t 1000 "Brightness Level" "%$RESULT_PERCENTAGE"

sudo tee /sys/class/backlight/*/brightness <<< $RESULT_BRIGHTNESS
