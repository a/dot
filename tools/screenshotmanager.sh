#!/bin/bash
# MIT licensed, based on https://gitlab.com/elixire/elixiremanager
readonly prog_name="screenshotmanager"
readonly prog_ver="8.0"

#defaults
mode=area
gimp=0

function usage() {
    echo "usage: ${1} [options]"
    echo "options:"
    echo "  -h, --help    print this help and exit"
    echo "  -v, --version print version number and exit"
    echo "      --check   check dependencies and exit"
    echo "  -a, --area    select an area to screenshot"
    echo "  -f, --full    screenshot the full screen"
    echo "  -g, --gimp    open file in gimp afterwards"
}

function version() {
    echo "$prog_name $prog_ver"
}

function check_deps() {
    for i in notify-send xclip; do
        (which $i &>/dev/null) && echo "OK: found $i" || echo "ERROR: $i not found"
    done

    (which escrotum &>/dev/null) && echo "OK: escrotum found" || echo "ERROR: escrotum not found"
}

function take_screenshot() {
    filename="/tmp/$(date +%s).png"
    flags="-C $filename"
    successful_message="Copied image."

    if [[ $gimp -eq 1 ]]; then
        flags="$filename && (gimp $filename &)"
        successful_message="Opened $filename in gimp."
    fi
    if [[ $1 == area ]]; then
        flags="-s $flags"
    fi

    bash -c "escrotum $flags"

    if [ $? -eq 0 ]
    then
        notify-send -t 5000 "$successful_message" -i "$filename" --hint=int:transient:1
    else
        notify-send -t 5000 "Error" "Got exit code $?" --hint=int:transient:1
    fi

    if [[ $gimp -ne 1 ]]; then
        \rm "$filename"
    fi
}

while [[ $# != 0 ]]; do
    case "${1}" in
        --help | -h)
            usage ${0}
            exit 0 ;;
        --version | -v)
            version ${0}
            exit 0 ;;
        --check)
            check_deps
            exit 0 ;;
        --area | -a)
            mode=area
            shift ;;
        --full | -f)
            mode=full
            shift ;;
        --gimp | -g)
            gimp=1
            shift ;;
        *)
            shift ;;
    esac
done

take_screenshot $mode
