#!/bin/python3
import os
import subprocess

# xkb layout name: Full name. Latter is only used on sway.
layouts = {"us": "English (US)", "tr": "Turkish"}

is_wayland = os.environ.get("XDG_SESSION_TYPE") == "wayland"
# Overriding this for, uh, reasons
is_wayland = True

def run_cmd(cmd):
    return subprocess.run(cmd,
                          capture_output=True,
                          shell=True).stdout.decode().strip()

def get_kbd():
    if is_wayland:
        return run_cmd('swaymsg -t get_inputs | jq "[.[] | select(.type==\\"keyboard\\")][0].xkb_layout_names[0]"').strip("\"")
    else:
        return run_cmd('setxkbmap -query '
                         '| grep layout '
                         '| cut -f 1 -d " " --complement | tr -d "[:space:]"')

current_layout = get_kbd()

if is_wayland:
    layout_index = list(layouts.values()).index(current_layout)
else:
    layout_index = list(layouts.keys()).index(current_layout)

print(list(layouts.keys())[layout_index])
