#!/bin/bash

bl_on() {
  bash change_brightness.sh 1
}

bl_off() {
  bash change_brightness.sh -1
  bash change_brightness.sh 50
}

process() {
  while read line; do
     case "$line" in
       UNBLANK*)
          bl_on
          ;;
       LOCK*)
          bl_off
          ;;
       BLANK*)
          bl_off
          ;;
     esac
  done
}

/usr/bin/xscreensaver-command -watch | process

