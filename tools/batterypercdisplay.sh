#!/bin/bash

batname="cw2015-battery"
maxlevel=98
warnlevel=15
batlevel=$(cat /sys/class/power_supply/$batname/capacity)
batstatus=$(cat /sys/class/power_supply/$batname/status)

# Print charging state
if [ $batstatus = "Charging" ]; then
	echo -n "⚡ "
fi

# Print battery icon
if [ $batlevel -gt $maxlevel ]; then
	batlevel=100
	echo -n ""
elif [ $batlevel -gt 75 ]; then
	echo -n ""
elif [ $batlevel -gt 50 ]; then
	echo -n ""
elif [ $batlevel -gt 25 ]; then
	echo -n ""
else
	echo -n ""
fi

# Print battery level
echo -n " $batlevel%"

# Send a notification if the battery is low
if [[ $batlevel -lt $warnlevel && $batstatus = "Discharging" ]]; then
	notify-send "Battery low!" "Charge your computer, your lazy dev."
fi
