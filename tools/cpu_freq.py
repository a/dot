#!/usr/bin/python3

with open("/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq") as f:
    max_cpu_freq = int(f.read()) / 1_000_000
print(f"{max_cpu_freq:g}GHz")
