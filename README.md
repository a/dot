# Ave's dotfiles

Heya, I have my dotfiles here.

If you want your system to look like mine (or if I somehow lose my configs), this is the place to go.

This repo includes submodules, please use --recursive when cloning!

## Screenshots

ZSH, tmux, sway, alacritty, neovim, working together:

![an image showcasing my sway, tmux, neovim and zsh config](screenshots/alltogethersway.png)

Sublime Text 3:

![ST3 showing an open file](screenshots/st3.png)

## Setup Instructions

First off, pick a WM betweeen awesomewm, i3 and sway (sway is wayland only). I use sway on my work computer and i3 on my personal Pinebook Pro.

#### Requirements

The entries marked with (LEGACY) are old configurations that I do not use anymore, and as such they may break things.

- For sway (if using Wayland): You need sway, swaylock, mako, rofi, waybar, and optionally slurp, wl-clipboard, grim. Also some tools rely on Python 3.6+.
- For i3 (if using X11): You need i3-gaps, i3lock, rofi, polybar, nitrogen, dunst and optionally xclip. Clone https://github.com/polybar/polybar-scripts/ to ~/Projects/ext/.
- For nvim: You need [dein](https://github.com/Shougo/dein.vim) installed at `~/.local/share/dein`.
- For zsh: You need oh-my-zsh or preferably, [zsher.sh](https://gist.githubusercontent.com/aveao/988b0e311d802a723ccdcd6d326c167b/raw/zsher.sh) (which installs oh-my-zsh)
- (LEGACY) For GTK: You need [breeze-gtk](https://www.archlinux.org/packages/extra/x86_64/breeze-gtk/).
- (LEGACY) For Qt: You need [qt5-styleplugins](https://www.archlinux.org/packages/community/x86_64/qt5-styleplugins/) and optionally, [breeze-icons](https://www.archlinux.org/packages/extra/any/breeze-icons/) (so that some apps render properly).
- (LEGACY) For awesomewm (if using X11): You need awesomewm 4.3+
- In general: You need a powerline font, pulseaudio and pamixer. For font, I use PragmataPro and it's hardcoded in certain parts. Iosevka is a good free alternative to PragmataPro.

#### Setup

The entries marked with (LEGACY) are old configurations that I do not use anymore, and as such they may break things.

- For sway (if on wayland): Copy (or symlink) `sway`, `rofi`, `waybar` and `mako` folders to `~/.config`. Copy `tools` folder under your home directory.
- For i3 (if on X11): Copy (or symlink) `i3`, `rofi`, `polybar`, `nitrogen` and `dunst` folders to `~/.config`. Copy `tools` folder under your home directory.
- For nvim: Copy (or symlink) `nvim/init.vim` to `~/.config/nvim/init.vim`, launch it once so that it installs the packages
- For zsh: Copy (or symlink) `zsh/.zshrc` to your home (`~`) folder. If you didn't use zsher you might need to install a couple packages, which I won't really help about.
- For tmux: Copy (or symlink) `tmux/.tmux.conf` to your home (`~`) folder
- For terminator: Copy the `terminator/config` file to `.config/terminator` folder
- For kitty: Copy the `kitty/kitty.conf` file to `.config/kitty` folder
- (LEGACY) For alacritty: Copy the `alacritty/alacritty.yml` file to `.config/alacritty` folder
- (LEGACY) For awesomewm (if on X): Copy (or symlink) the `awesome` folder to `~/.config`
- (LEGACY) For GTK: Copy the `gtk-3.0/settings.ini` to `.config/gtk-3.0` folder
- (LEGACY) For Qt: Add `export QT_QPA_PLATFORMTHEME=gtk2` lines to end of `/etc/profile`, source it (`. /etc/profile`).
- (LEGACY) For lightdm-gtk-greeter (if on X): Copy `lightdm-gtk-greeter/lightdm-gtk-greeter.conf` to `/etc/lightdm/lightdm-gtk-greeter.conf` and pngs in `lightdm-gtk-greeter` folder to `/var/ffa` (I have a 777 folder called `/var/ffa` because permissions are hard)
- (LEGACY) For Xscreensaver (only used on awesomewm): Copy `lightdmbg.png` in `lightdm-gtk-greeter` folder to `/var/ffa/bg`, copy `xscreensaver/.xscreensaver` to your home (`~`) folder.
- For tools (set of shell scripts): Copy `tools` folder to your home folder

## Sublime Text 3 Configuration

My preferred editor is ST3. ~~I currently don't have a license and just press Esc when it does the popup thing.~~ I paid a soul and half and now I have a license. ST3 is cool.

You'll probably want to get [Package Control](https://packagecontrol.io/) before installing any packages.

#### Packages

- Anaconda
- Auto Close HTML Tags
- BracketHighlighter
- Color Highlighter
- DA UI (which is no longer available, [extract this](https://github.com/aveao/sublime-da-ui/releases/tag/asd) to `~/.config/sublime-text-3/Packages/`)
- EditorConfig
- Elixir
- FileIcons
- GitGutter
- Golang Tools Integration
- nginx
- Package Control
- Pretty JSON
- Sass
- Sidebarenhancements
- SublimeLinter
- SublimeLinter-annotations
- SyncedSideBar

#### Additional Configs

- Set DA UI's Origin as skin and Mahogany Red as accent
- For Anaconda, use the following user config:

```js
{
    "pep8_max_line_length": 80,
    "anaconda_gutter_theme": "dark",
}
```

- Here's my key bindings:

```js
[
    { "keys": ["alt+right"], "command": "next_view" },
    { "keys": ["alt+left"], "command": "prev_view" },
    { "keys": ["alt+x"], "command": "close_file" },
]
```

- Here's the whole settings file:

```js
{
	"always_show_minimap_viewport": true,
	"auto_complete_commit_on_tab": true,
	"auto_complete_with_fields": true,
	"bold_folder_labels": true,
	"color_scheme": "Packages/DA UI/DA Dark.tmTheme",
	"detect_indentation": false,
	"font_face": "Pragmata Pro",
	"font_size": 10,
	"ignored_packages":
	[
		"CSS",
		"Markdown",
		"Vintage"
	],
	"line_padding_bottom": 3,
	"line_padding_top": 3,
	"material_theme_accent_graphite": true,
	"overlay_scroll_bars": "enabled",
	"skin": "DA UI/Origin",
	"tab_size": 4,
	"theme": "DA.sublime-theme",
	"translate_tabs_to_spaces": true
}
```

## Notes

- For MPRIS with MPD, I use mpDris2: https://github.com/eonpatapon/mpDris2

## Credit

- lcpz for [awesome-copycats](https://github.com/lcpz/awesome-copycats), which is what my awesomewm configs are based on
- blueyed for [awesome-cyclefocus](https://github.com/blueyed/awesome-cyclefocus), which is used on my awesomewm setup
- prugrio for [darkcloud-tmuxconfig](https://github.com/prurigro/darkcloud-tmuxconfig), which is what my tmux configs are based on
- oh-my-zsh contributors and theme creators for being great in general, and also because my zsh configs require it.
- Shougo for [dein](https://github.com/Shougo/dein.vim), which is required by nvim config
- Smyck for [smyck color scheme](http://color.smyck.org/), which is used on Terminator and Alacritty as the color scheme

## License

- License of the project is preserved if the project in question is copyleft.
- MIT for the rest of the code I wrote.
- CC-BY-NC for the text, documentation, configs.
