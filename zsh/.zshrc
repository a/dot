if [ "$TMUX" = "" ]; then tmux new-session -A -s main; fi

# fortune cookie
fortune -s

#export TERM="xterm-256color"
export LANG=en_US.UTF-8
export ZSH=/home/ave/.oh-my-zsh

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

ZSH_THEME="powerlevel10k/powerlevel10k"
DEFAULT_USER="ave"
UPDATE_ZSH_DAYS=30
COMPLETION_WAITING_DOTS="true"
plugins=(git common-aliases git-extras screen sudo history zsh-completions zsh-syntax-highlighting zsh-autosuggestions)
autoload -U compinit && compinit
source $ZSH/oh-my-zsh.sh
# POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir rbenv vcs)
# POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status command_execution_time root_indicator ssh time)
# POWERLEVEL9K_TIME_FORMAT="%D{%I:%M:%S %p}"
alias vim="nvim"
export EDITOR='nvim'
export SUDO_EDITOR='nvim'
alias e="$EDITOR"
alias se="sudoedit"

alias amdrun="DRI_PRIME=1"

manpdf() 
{ 
	man -t $1 | ps2pdf - /tmp/$1.pdf
	okular /tmp/$1.pdf & 
}

cl()
{
	cd $1
	ls
}

elixireactivate()
{
	curl -X POST -H 'Authorization: <non>' https://elixi.re/api/admin/activate/$1
}

elixireadd()
{
	  curl -X POST -H "X-Auth-Key: <nop>" -H "X-Auth-Email: <nuh uh>" -H "Content-Type: application/json" "https://api.cloudflare.com/client/v4/zones" --data '{"name":"'$1'","jump_start":true}'
}

elixireban()
{
curl -X POST "https://api.cloudflare.com/client/v4/user/firewall/access_rules/rules" \
     -H "X-Auth-Email: <nop>" \
     -H "X-Auth-Key: <nuh uh>" \
     -H "Content-Type: application/json" \
     --data "{\"mode\":\"block\",\"configuration\":{\"target\":\"ip\",\"value\":\"$1\"},\"notes\":\"banned through cli\"}"
}

alias elixiremanager="/home/ave/Documents/elixire.sh"

viewcert()
{
    echo | openssl s_client -servername $1 -connect $1:443 2>/dev/null | openssl x509 -text
}

blockdump()
{
    echo "Cert:\n"
    echo | openssl s_client -servername $1 -connect $1:443 2>/dev/null | openssl x509 -text
    echo "\n\nDig:\n"
    dig $1
    echo "\n\ncurl results for http:\n"
    curl -k http://$1
    echo "\n\ncurl for https:\n"
    curl -k https://$1
    echo "\n\nwhois for the site:\n"
    whois -H $1
    IP="$(dig +short $1 | tail -n 1)"
    echo "\n\nwhois for the IP ($IP)\n"
    whois -H $IP
}

function megamanager()
{
	shasum=$(sha1sum $1 | awk '{print $1}')
	b2 upload-file --sha1 $shasum avepub $1 $1
}

# http://superuser.com/questions/611538/ddg#611582
function countdown(){
   date1=$((`date +%s` + $1)); 
   while [ "$date1" -ge `date +%s` ]; do 
     echo -ne "$(date -u --date @$(($date1 - `date +%s`)) +%H:%M:%S)\r";
     sleep 0.1
   done
}
function stopwatch(){
  date1=`date +%s`; 
   while true; do 
    echo -ne "$(date -u --date @$((`date +%s` - $date1)) +%H:%M:%S)\r"; 
    sleep 0.1
   done
}


export WINEDEBUG=-all

alias qgits="git config --global commit.gpgsign false && git add . && git commit && git push && git config --global commit.gpgsign true"
alias qgit="git add . && git commit && git push"
alias pbcopy='xclip -selection c'
alias pbpaste='xclip -selection clipboard -o'
alias rec='asciinema rec'
alias mouse='sudo rmmod i2c_hid;sudo modprobe i2c_hid'

alias fusee="python3 /home/ave/Projects/fusee-launcher/fusee-launcher.py"

alias joycon="sudo /home/ave/go/bin/jcdriver"
alias jcxpad="sudo xboxdrv --evdev /dev/input/event21 --evdev-absmap ABS_Z=x1,ABS_RX=y1,ABS_RY=x2,ABS_RZ=y2 --axismap -X1=X1,-X2=X2 --evdev-keymap BTN_EAST=b,BTN_SOUTH=a,BTN_WEST=y,BTN_NORTH=x,BTN_THUMBR=tr,BTN_THUMBL=tl,BTN_SELECT=back,BTN_MODE=guide,BTN_MODE=guide,BTN_START=start,BTN_TL=lb,BTN_TL2=lt,BTN_TR=rb,BTN_TR2=rt --mimic-xpad --silent"
alias zshrcr="$EDITOR ~/.zshrc && source ~/.zshrc"
alias getclass="xprop WM_CLASS WM_NAME"

transfer() { if [ $# -eq 0 ]; then echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi
tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile; else curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ; fi; cat $tmpfile; rm -f $tmpfile; } 

DEVKITPRO=/opt/devkitpro
DEVKITARM=/opt/devkitpro/devkitARM
DEVKITPPC=/opt/devkitpro/devkitPPC

alias hactool="hactool --disablekeywarns"
alias 010editor="QT_QPA_PLATFORM=xcb 010editor"
alias obs="QT_QPA_PLATFORM=xcb obs"
alias genymotion="QT_QPA_PLATFORM=xcb genymotion"
alias charles="_JAVA_AWT_WM_NONREPARENTING=1 charles"
alias arduino="_JAVA_AWT_WM_NONREPARENTING=1 arduino"
alias virtualbox="QT_QPA_PLATFORM=xcb virtualbox"
alias xterm='XENVIRONMENT="${HOME}/.Xresources" xterm'

# fuzzy finding commands
alias cdf='cd $(fzf)'
alias sublf='subl $(fzf)'
alias vf='vim $(fzf)'

export DOTNET_CLI_TELEMETRY_OPTOUT=1
alias wanip='dig +short myip.opendns.com @resolver1.opendns.com'
export PATH=$PATH:$HOME/.dvm/sym
export PATH=$PATH:$HOME/tools

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
