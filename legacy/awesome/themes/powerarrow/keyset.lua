local awful = require("awful")
local beautiful = require("beautiful")
local spawn = require("awful.spawn")
local watch = require("awful.widget.watch")
local wibox = require("wibox")

local KEYBOARD_CONFIGS = {"us", "tr"}

local GET_KB_CMD = 'bash -c \'setxkbmap -query | grep layout | cut -f 1 -d " " --complement | tr -d "[:space:]"\''
local SET_KB_CMD = 'setxkbmap '
local CURRENT_INDEX = 1

local keyset_display = wibox.widget {
    id = "txt",
    align = "center",
    font = "Pragmata Pro 10",
    widget = wibox.widget.textbox
}

local update_graphic = function(widget, stdout, _, _, _)
    keyset_display.text = stdout
end

local change_keyboard = function()
    CURRENT_INDEX = CURRENT_INDEX + 1
    if CURRENT_INDEX > #KEYBOARD_CONFIGS then
        CURRENT_INDEX = 1
    end
    awful.spawn(SET_KB_CMD .. KEYBOARD_CONFIGS[CURRENT_INDEX], false)
end

keyset_display:connect_signal("button::press", function(_, _, _, button)
    if (button == 1) then change_keyboard() 
    end

    spawn.easy_async(GET_KB_CMD, function(stdout, stderr, exitreason, exitcode)
        update_graphic(keyset_display, stdout, stderr, exitreason, exitcode)
    end)
end)

watch(GET_KB_CMD, 1, update_graphic, keyset_display)

-- Change keyboard to index 0 on start, hacky way to set default keyboard ig
awful.spawn(SET_KB_CMD .. KEYBOARD_CONFIGS[CURRENT_INDEX], false)

return keyset_display
