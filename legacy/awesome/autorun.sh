#!/usr/bin/env bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

function run2 {
  if ! pgrep $1 ;
  then
    ${@:2}&
  fi
}

run xscreensaver &
#run ~/Projects/lightsonplus/lightson+ &
#run gajim &
run2 firefox firefox-developer-edition &
run thunderbird &
run kdeconnect-indicator &
#run bitwarden-bin &
#run keepassxc &
#run2 ~/tools/xscreensaver-blank.sh &
